# Blackjack python implementation

import time
import random

class Card(object):
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank
        self.face_up = True

class Hand(object):
    def __init__(self):
        self.cards = []
        self.card_values = {"A": 11, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "J": 10, "Q": 10, "K": 10}

    def addCard(self, card):
        self.cards.append(card)

    def getHandValue(self):
        num_of_aces = 0
        total_value = 0
        for card in self.cards:
            if card.rank == "A":
                num_of_aces += 1
            total_value += self.card_values[card.rank]
        while num_of_aces > 0 and total_value > 21:
            total_value -= 10
            num_of_aces -= 1
        return total_value

    def showCards(self):
        hand_list = []
        for card in self.cards:
            if card.face_up:
                hand_list.append(card.rank + card.suit)
            else:
                hand_list.append("Hidden")
        return ",".join(hand_list)

class Deck(object):
    def __init__(self):
        suits = ["C", "D", "H", "S"]
        ranks = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]

        self.deck = []

        for suit in suits:
            for rank in ranks:
                self.deck.append(Card(suit, rank))

    def getNextCard(self):
        return self.deck.pop()

    def shuffle(self):
        random.shuffle(self.deck)

class Player(object):
    def __init__(self, name="", balance=100):
        self.hand = Hand()
        self.balance = balance
        self.bet_amount = 0
        self.name = name
        self.is_busted = False

    def hit(self, card):
        self.hand.addCard(card)

    def initializeHand(self, deck):
        self.hand = Hand()
        self.hit(deck.getNextCard())
        self.hit(deck.getNextCard())
        self.is_busted = False

class Game(object):
    def __init__(self, minimum_bet=1):
        self.minimum_bet = minimum_bet

        self.players = []
        num_players = self.inputValidator("Please enter the number of players (1-5): \n", valid_range = [1,5])
        for i in range(1, num_players+1):
            self.players.append(Player("Player " + str(i)))
        self.dealer = Player()

        continue_game = "Y"
        while continue_game == "Y":
            self.deck = Deck()
            self.deck.shuffle()

            self.takeBets()
            self.dealCards()
            self.playersMove()
            self.dealersMove()
            self.resolveGame()

            if len(self.players) == 0:
                print "All the players in this room have emptied their pots. The game has ended."
                break

            continue_game = self.inputValidator("Would you like to continue playing? Type Y for Yes or N for No ", ["Y", "N"])
            if continue_game == "Y":
                print "---------------------------------"
                print "Starting a new round!"
        print "Thanks for playing Blackjack!"
        return

    """
    Collect the bet amount from each player.
    """
    def takeBets(self):
        print "\nTaking bets from all players."
        time.sleep(2)

        for player in self.players:
            print player.name + ", Your current balance is: " + str(player.balance) + " chips"
            bet_amount = self.inputValidator("How much would you like to bet?\n", valid_range=[self.minimum_bet, player.balance])
            player.bet_amount = bet_amount
            player.balance -= bet_amount

    """
    Provide two cards to each player and dealer. Dealer's second card is kept face down.
    """
    def dealCards(self):
        print "\nDealing cards..."
        time.sleep(2)

        for player in self.players:
            player.initializeHand(self.deck)
        self.dealer.initializeHand(self.deck)
        self.dealer.hand.cards[-1].face_up = False
        print "Dealer's hand is: " + self.dealer.hand.showCards()

    """
    For each player still active, record their moves and update the hand accordingly.
    """
    def playersMove(self):
        for player in self.players:
            while player.hand.getHandValue() <= 21:
                print player.name + ", your hand is: " + player.hand.showCards()
                response = self.inputValidator(player.name + ": What would you like to do? Type S to Stand or H to Hit\n", ["H", "S"])
                if response == "H":
                    player.hit(self.deck.getNextCard())
                    if player.hand.getHandValue() > 21:
                        player.is_busted = True
                        print "You just busted! Your hand is: " + player.hand.showCards() + ". Your balance is: " + str(player.balance) + " chips"
                        break
                elif response == "S":
                    break

    """
    Dealer makes their move
    """
    def dealersMove(self):
        print "\nWaiting for the dealer..."
        time.sleep(2)

        self.dealer.hand.cards[-1].face_up = True   # Flip the hidden card
        while self.dealer.hand.getHandValue() < 17 and self.dealer.hand.getHandValue() <= 21 :
            print "Dealer's hand is: " + self.dealer.hand.showCards()
            print "Dealer is going to hit"
            self.dealer.hit(self.deck.getNextCard())
        if self.dealer.hand.getHandValue() > 21:
            print "The dealer just busted! The dealer's hand is: " + self.dealer.hand.showCards() + ". The hand's value is " + str(self.dealer.hand.getHandValue())
            self.dealer.is_busted = True
        else:
            print "The dealer chooses to stand. The dealer's hand is: " + self.dealer.hand.showCards() + ". The hand's value is " + str(self.dealer.hand.getHandValue())

    """
    Arrange the settlement based on the players' and dealer's hand.
    """
    def resolveGame(self):
        print "\nIt's time for the settlement!"
        time.sleep(3)

        temp_players = list(self.players)
        for player in self.players:
            if not player.is_busted:
                if self.dealer.is_busted or player.hand.getHandValue() > self.dealer.hand.getHandValue():
                    earnings = player.bet_amount * 2
                    player.balance += earnings
                    print player.name + " beat the dealer and just earned " + str(earnings) + ". Your current balance is: " + str(player.balance) + " chips"
                elif player.hand.getHandValue() == self.dealer.hand.getHandValue():
                    print player.name + " tied with the dealer. You get your initial bet back."
                    player.balance += player.bet_amount
                else:
                    print player.name + " loses to the dealer. You lose " + str(player.bet_amount) + " this round."
            if player.balance < 1:
                # If player runs out of chips, remove from list of players.
                print player.name + ", you have lost all of your chips. You have been kicked out of this room."
                temp_players.remove(player)
            else:
                print player.name + ", your balance is: " + str(player.balance) + " chips"
        self.players = temp_players

    """
    Helper function to handle user input
    """
    def inputValidator(self, output_text, valid_values=None, valid_range=None):
        value = None
        while True:
            value = raw_input(output_text)
            if valid_values is not None:
                value = value.upper()
                if value not in valid_values:
                    print "You entered an invalid input. Please try again."
                else:
                    break
            elif valid_range is not None:
                try:
                    value = int(value)
                    if value < valid_range[0] or value > valid_range[1]:
                        print "You entered an invalid input. Please enter a number between " + str(valid_range[0]) + " and " + str(valid_range[1])
                    else:
                        break
                except ValueError:
                    print "You entered an invalid input. Please enter a number between " + str(valid_range[0]) + " and " + str(valid_range[1])
            else:
                break
        return value

def main():
    new_game = ""
    while new_game != "Y" and new_game != "N":
        new_game = raw_input("Welcome to blackjack. Would you like to start a new game? Reply Y or N\n").upper()
        if new_game == "Y":
            print "Awesome. Let's start a new game!"
            Game()
        elif new_game == "N":
            print "Sucks to see you go!"
        else:
            print "You entered an invalid input. Please type Y for Yes or N for No.\n"

if __name__ == '__main__':
    main()
