# Blackjack #

### How to play ###

To get started, use the following instructions:

* Clone the blackjack respository

`git clone https://manavm77@bitbucket.org/manavm77/blackjack.git`

* Navigate into the blackjack directory

`cd blackjack/`

* Use the command below to start a new game

`python blackjack.py`

### Card representation ###

Hands are represented as [RANK][SUIT]. Therefore, an Ace of Hearts will be represented as "AH".

Example hand: 7D(Seven of Diamonds), 6H(Six of Hearts)

### Rules of the game ###

* Each game can have anywhere from 1 to 5 players.
* Each player is given 100 chips to start the game. The minimum bet for each round is 1 chip. Once a player loses all their chips, they are kicked out of the room. Once all the players have been kicked out of the room, the game ends.
* Once all the bets have been placed, cards are dealt first to all the players, and then to the dealer. The dealer has one card hidden.
* Each player has the option to either hit or stand. If a player busts (has a hand greater than 21), the player loses their wager. The dealer moves to the next player once a player busts or stands.
* Once all the players have played their moves, the dealer will flip the hidden card. The dealer has to continue to hit until their hand is worth at least 17 or if they bust. Once the dealer goes past 17, they will choose to stand.
* At the end of each round, there's two possibilities:
	1. If the dealer busts, every player that's still standing gets a 2x return on their original wager.
	2. If the dealer does not bust, every player that's still standing and has a total greater than the dealer gets 2x their original wager. Otherwise, the player loses their wager.

### Design ###

My implementation has the following entities:

#### Card ####

A card object has two primary properties: suit and rank. It also has a `face_up` property to determine whether a card is placed face up on the table.

#### Hand ####

A hand object contains a list of cards. It has the following functions

* `addCard()`: Adds a card to hand
* `getHandValue()`: Returns the value of the hand
* `showCards()`: Prints out the hand in comma-separated format

#### Deck ####

A Deck object represents a deck of 52 cards. It contains a list of Card objects. It has the following functions:

* `getNextCard()`: Returns the top card from the deck
* `shuffle()`: Shuffles the deck of cards

#### Player ####

A Player object represents a player playing the game. It contains the following properties:

* `hand`: A hand containing a list of cards
* `balance`: The amount of chips in a player's pot. Initialized to 100 chips.
* `bet_amount`: The amount of chips a player bets in a given round
* `name`: Name of the player
* `is_busted`: Boolean to check if the player has busted in a given round

It also has the following functions:

* `hit()`: Adds a card to the player's hand
* `initializeHand()`: Adds two cards to the player's hand

#### Game ####

A Game object represents the gameplay for Blackjack. It has the following properties:

* `minimum_bet`: The minimum amount of chips to play each round
* `players`: A list of players that will be playing the game
* `dealer`: An instantiation of the Player class that represents the dealer

The core gameplay is supported by the following functions:

* `takeBets()`: Collect the bet amount from each player and set the `bet_amount` property.
* `dealCards()`: Provide two cards to each player and dealer. Dealer's second card is kept face down.
* `playersMove()`: For each player still active, record their moves and update the hand accordingly.
* `dealersMove()`: Once all the players are done, the dealer makes their move.
* `resolveGame()`: Arrange the settlement based on the players' and dealer's hand.

### Future Improvements ###

* Inclusion of other moves such as splitting, doubling down, and insurance.
* Improving user experience by building a better interface, and allowing player profiles. 
* Building a test suite to test the modules
* Adding a persistence layer that would allow features like leaderboards and players being involved in multiple games. 
* A storage layer could also allow analytics being surfaced through a dashboard.